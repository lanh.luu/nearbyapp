//
//  SceneDelegate.h
//  NEARBY IOS
//
//  Created by mptt2 on 11/18/19.
//  Copyright © 2019 MSERVICE JSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

